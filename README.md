# HP42 Calculator Program Decoder

HP42 calculator programs are often distributed as .raw files.  One can of
course load a program into an emulator like Free42 or Plus42 and then copy the
decoded text to a file, but I wanted a command-line tool that could do the
conversion.

I predominantly use Linux, and this repository contains a simple tool that
can do the conversion. 

It expects one .raw file as input, and it writes the decoded program to
standard output.

If you want to also see a hexadecimal representation of the program, add a
-x flag to the command.

For example:

```
dm42decode -x foo.raw > foo.txt
```

will decode the `foo.raw` binary into `foo.txt` along with the individual
hex bytes that correspond to each program line.

## Compiling

This program doesn't have any exotic requirements, and it should compile
by simply typing `make`.

## Status

I've tested this program and it produces correct results for the various
HP42 programs that I've found on the 'net.

