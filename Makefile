# DM-42 decoder
#
# Copyright (c) 2023 Steven A. Falco
# 
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.  This program is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this
# program. If not, see <https://www.gnu.org/licenses/>.

OBJDIR			:= build

LIBS			:=

DIRS			:= $(OBJDIR)

CFLAGS			+= -O2 -Wall -Werror -MD

LFLAGS			+=

SOURCES			:=						\
			debug.c						\
			fileOps.c					\
			main.c						\
			#

OBJECTS			:= $(SOURCES:%.c=$(OBJDIR)/%.o)

LIBLS			:= $(LIBS:%=-l%)
LIBDEPS			:= $(LIBS:%=$(LIBDIR)/lib%.a)

all:	$(DIRS)								\
	$(OBJDIR)/dm42decode

install: all
	cp $(OBJDIR)/dm42decode ~/bin

$(OBJDIR)/dm42decode: $(OBJECTS) $(LIBDEPS)
	$(announce)
	$(CC) $(CFLAGS) $(LFLAGS) -o $@ $(OBJECTS) $(LIBLS)

$(OBJDIR)/%.o:		%.c
	$(announce)
	$(CC) $(CFLAGS) -c -o $@ $(abspath $<)
	$(c_fix_depend)

$(DIRS):
	[ -d $(@) ] || mkdir -p $(@)

clean:
	$(announce)
	rm -fr $(OBJDIR)

# Include dependencies
include $(wildcard $(OBJDIR)/*.d)

announce = @echo; \
	   if [ -f "$@" ]; \
	   then echo "* $@: $?"; \
	   else echo "* $@:"; \
	   fi; \
	   echo

define c_fix_depend
	@-sed -e 's!^$(notdir $(<:%.c=%.o))!$(@)!' \
	  < $(OBJDIR)/$(notdir $(<:%.c=%.d)) \
	  > $(OBJDIR)/$(notdir $(<:%.c=%.d)).tmp
	@-mv -f $(OBJDIR)/$(notdir $(<:%.c=%.d)).tmp \
	        $(OBJDIR)/$(notdir $(<:%.c=%.d))
endef

define c_fix_depend_shared
	@-sed -e 's!^$(notdir $(<:%.c=%.o))!$(@)!' \
	  < $(OBJDIR)/shared/$(notdir $(<:%.c=%.d)) \
	  > $(OBJDIR)/shared/$(notdir $(<:%.c=%.d)).tmp
	@-mv -f $(OBJDIR)/shared/$(notdir $(<:%.c=%.d)).tmp \
	        $(OBJDIR)/shared/$(notdir $(<:%.c=%.d))
endef
