// DM-42 decoder
//
// Copyright (c) 2023-2024 Steven A. Falco
// 
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _FILE_OPS_H_
#define _FILE_OPS_H_

#include <stdint.h>

void fileLoad(char *iName);

uint8_t getNextByte();
uint8_t getByte(size_t where);

void pushBack();

size_t getPosition();
void setPosition(size_t where);

size_t sizeRemaining();

#endif // _FILE_OPS_H_

