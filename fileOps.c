// DM-42 decoder
//
// Copyright (c) 2023-2024 Steven A. Falco
// 
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>

#include <sys/stat.h>

#include "fileOps.h"

#include "debug.h"

uint8_t *iFile;
uint8_t *iPtr;
uint8_t *savePtr;
uint8_t *iLast;

size_t iLength;

void
fileLoad(char *iName)
{
	int iFd;

	if((iFd = open(iName, O_RDONLY)) == -1) {
		fprintf(stderr, "Cannot open %s for reading\n", iName);
		exit(EXIT_FAILURE);
	}

	struct stat sb;

	if(fstat(iFd, &sb) != 0) {
		fprintf(stderr, "Cannot stat %s\n", iName);
		exit(EXIT_FAILURE);
	}
	iLength = sb.st_size;

	if((iFile = malloc(iLength)) == NULL) {
		fprintf(stderr, "Cannot allocate memory for %s\n", iName);
		exit(EXIT_FAILURE);
	}

	if((read(iFd, iFile, iLength)) != iLength) {
		fprintf(stderr, "Cannot read %s\n", iName);
		exit(EXIT_FAILURE);
	}

	close(iFd);

	iPtr = iFile;
	iLast = iFile + iLength;
}

uint8_t
getNextByte()
{
	if(iPtr >= iLast) {
		DEBUG_printf("Premature EOF at byte %ld", iPtr - iFile);
		exit(EXIT_FAILURE);
	}

	return *iPtr++;
}

uint8_t
getByte(size_t where)
{
	if(where < iLength) {
		return iFile[where];
	}

	DEBUG_printf("Offset out of range at %ld %ld", where, iLength);
	exit(EXIT_FAILURE);
}

void
pushBack()
{
	if((iPtr - iFile) > 0) {
		--iPtr;
	}
}

size_t
getPosition()
{
	return iPtr - iFile;
}

void
setPosition(size_t where)
{
	iPtr = iFile + where;
}

size_t
sizeRemaining()
{
	return iLast - iPtr;
}

