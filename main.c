// DM-42 decoder
//
// Copyright (c) 2023-2024 Steven A. Falco
// 
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include "debug.h"
#include "fileOps.h"

#define GOOSE		"▸"

// Some .raw files contain more than one program.  We need to know the length
// of each program in order to emit the 00 line, hence we use two passes.
//
// In the first pass we do the parse in order to locate the end of a program.
// This tells us the length of a program.
//
// In the second pass we emit the text corresponding to the program, since
// we now know its length.
//
// If there is still unprocessed data in the file, we loop to repeat the two
// passes for the next program, and so on until we hit the end of the file.
//
// To facilitate using multiple passes, we load the entire file into memory
// rather than rewinding and re-reading the file.
#define FIRST_PASS	0
#define SECOND_PASS	1

#define END_LENGTH	3	// Length in bytes of "END" statement.
#define BIG_BUF		65536

int curLine = 0;

// This is wacky.  XSTR can be a lot longer than the normal 14 character limit
// so we need all kinds of hacks.
//
// Similarly numbers with suffixes are wacky.
bool suppressor = false;
int extraLen = 0;

// Flags.
bool wantHex = false;

size_t
utf8len(const char *s)
{
	size_t len = 0;

	while(*s) {
		len += ((*s++ & 0xC0) != 0x80);
	}

	return len;
}

char buffer[BIG_BUF];
char *bufferPtr = buffer;

void
bufferReset()
{
	bufferPtr = buffer;
}

void bufferAdd(const char *format, ...) __attribute__ ((format (printf, 1, 2)));

void
bufferAdd(const char *format, ...)
{
	va_list ap;
	int len;
	int remaining = (buffer + BIG_BUF) - bufferPtr;

	va_start(ap, format);
	len = vsnprintf(bufferPtr, remaining, format, ap);
	va_end(ap);

	bufferPtr += len;
}

int
bufferLen()
{
	return utf8len(buffer);
}

void
bufferEmit(int pass, bool wantHex, size_t start, size_t end)
{
	if(pass == SECOND_PASS) {
		if(wantHex && !suppressor) {
			int pos = 40 - utf8len(buffer) - extraLen;
			printf("%s%*s", buffer, pos, "");
			while(start < end) {
				printf("%02x ", getByte(start++));
			}
		} else {
			printf("%s", buffer);
		}
	}

	if(!suppressor) {
		if(pass == SECOND_PASS) {
			printf("\n");
		}
		extraLen = 0;
	}

	bufferReset();
}

char *
translate(uint8_t v)
{
	switch(v) {
		case 0x00: return "÷";
		case 0x01: return "×";
		case 0x02: return "√";
		case 0x03: return "∫";
		case 0x04: return "░";
		case 0x05: return "Σ";
		case 0x06: return "▸";
		case 0x07: return "π";
		case 0x08: return "¿";
		case 0x09: return "≤";
		case 0x0a: return "[LF]";
		case 0x0b: return "≥";
		case 0x0c: return "≠";
		case 0x0d: return "↵";
		case 0x0e: return "↓";
		case 0x0f: return "→";
		case 0x10: return "←";
		case 0x11: return "μ";
		case 0x12: return "£";
		case 0x13: return "°";
		case 0x14: return "Å";
		case 0x15: return "Ñ";
		case 0x16: return "Ä";
		case 0x17: return "∡";
		case 0x18: return "ᴇ";
		case 0x19: return "Æ";
		case 0x1a: return "…";
		case 0x1b: return "␛";
		case 0x1c: return "Ö";
		case 0x1d: return "Ü";
		case 0x1e: return "▒";
		case 0x1f: return "•";
		case 0x20: return " ";
		case 0x21: return "!";
		case 0x22: return "\"";
		case 0x23: return "#";
		case 0x24: return "$";
		case 0x25: return "%";
		case 0x26: return "&";
		case 0x27: return "'";
		case 0x28: return "(";
		case 0x29: return ")";
		case 0x2a: return "*";
		case 0x2b: return "+";
		case 0x2c: return ",";
		case 0x2d: return "-";
		case 0x2e: return ".";
		case 0x2f: return "/";
		case 0x30: return "0";
		case 0x31: return "1";
		case 0x32: return "2";
		case 0x33: return "3";
		case 0x34: return "4";
		case 0x35: return "5";
		case 0x36: return "6";
		case 0x37: return "7";
		case 0x38: return "8";
		case 0x39: return "9";
		case 0x3a: return ":";
		case 0x3b: return ";";
		case 0x3c: return "<";
		case 0x3d: return "=";
		case 0x3e: return ">";
		case 0x3f: return "?";
		case 0x40: return "@";
		case 0x41: return "A";
		case 0x42: return "B";
		case 0x43: return "C";
		case 0x44: return "D";
		case 0x45: return "E";
		case 0x46: return "F";
		case 0x47: return "G";
		case 0x48: return "H";
		case 0x49: return "I";
		case 0x4a: return "J";
		case 0x4b: return "K";
		case 0x4c: return "L";
		case 0x4d: return "M";
		case 0x4e: return "N";
		case 0x4f: return "O";
		case 0x50: return "P";
		case 0x51: return "Q";
		case 0x52: return "R";
		case 0x53: return "S";
		case 0x54: return "T";
		case 0x55: return "U";
		case 0x56: return "V";
		case 0x57: return "W";
		case 0x58: return "X";
		case 0x59: return "Y";
		case 0x5a: return "Z";
		case 0x5b: return "[";
		case 0x5c: return "\\";
		case 0x5d: return "]";
		case 0x5e: return "↑";
		case 0x5f: return "_";
		case 0x60: return "`";
		case 0x61: return "a";
		case 0x62: return "b";
		case 0x63: return "c";
		case 0x64: return "d";
		case 0x65: return "e";
		case 0x66: return "f";
		case 0x67: return "g";
		case 0x68: return "h";
		case 0x69: return "i";
		case 0x6a: return "j";
		case 0x6b: return "k";
		case 0x6c: return "l";
		case 0x6d: return "m";
		case 0x6e: return "n";
		case 0x6f: return "o";
		case 0x70: return "p";
		case 0x71: return "q";
		case 0x72: return "r";
		case 0x73: return "s";
		case 0x74: return "t";
		case 0x75: return "u";
		case 0x76: return "v";
		case 0x77: return "w";
		case 0x78: return "x";
		case 0x79: return "y";
		case 0x7a: return "z";
		case 0x7b: return "{";
		case 0x7c: return "|";
		case 0x7d: return "}";
		case 0x7e: return "~";
		case 0x7f: return "├";
		default: return "";
	}
}

char *
decodeNN(uint8_t val, int places)
{
	static char buf[256];

	switch(val) {
		case 0x66: sprintf(buf, "A");		break;
		case 0x67: sprintf(buf, "B");		break;
		case 0x68: sprintf(buf, "C");		break;
		case 0x69: sprintf(buf, "D");		break;
		case 0x6a: sprintf(buf, "E");		break;
		case 0x6b: sprintf(buf, "F");		break;
		case 0x6c: sprintf(buf, "G");		break;
		case 0x6d: sprintf(buf, "H");		break;
		case 0x6e: sprintf(buf, "I");		break;
		case 0x6f: sprintf(buf, "J");		break;
		case 0x70: sprintf(buf, "ST T");	break;
		case 0x71: sprintf(buf, "ST Z");	break;
		case 0x72: sprintf(buf, "ST Y");	break;
		case 0x73: sprintf(buf, "ST X");	break;
		case 0x74: sprintf(buf, "ST L");	break;
		case 0x7b: sprintf(buf, "a");		break;
		case 0x7c: sprintf(buf, "b");		break;
		case 0x7d: sprintf(buf, "c");		break;
		case 0x7e: sprintf(buf, "d");		break;
		case 0x7f: sprintf(buf, "e");		break;
		case 0xf0: sprintf(buf, "IND ST T");	break;
		case 0xf1: sprintf(buf, "IND ST Z");	break;
		case 0xf2: sprintf(buf, "IND ST Y");	break;
		case 0xf3: sprintf(buf, "IND ST X");	break;
		case 0xf4: sprintf(buf, "IND ST L");	break;

		default:
			   if(val < 0x80) {
				   sprintf(buf, "%0*d", places, val);
			   } else {
				   sprintf(buf, "IND %0*d", places, val - 0x80);
			   }
			   break;
	}

	return buf;
}

void
parseShortLabel(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d%sLBL %s", curLine++, GOOSE, decodeNN(chr, 2));
}

bool
parseLabel(uint8_t op)
{
	int len;
	int i;
	uint8_t chr;

	getNextByte(); // Toss the next byte.

	// Normally, we expect END to be 0xc0, 0x00, 0x0d, but I've seen one
	// example that violates that.  So instead, we treat all bytes that
	// are not valid length specifiers to indicate END.
	if((chr = getNextByte()) < 0xf1) {
		bufferAdd("%02d END", curLine++);
		return 1; // This program is complete.
	}
	len = (chr & 0x0f) - 1;

	getNextByte(); // Toss the next byte.

	bufferAdd("%02d%sLBL \"", curLine++, GOOSE);
	for(i = 0; i < len; i++) {
		bufferAdd("%s", translate(getNextByte()));
	}
	bufferAdd("\"");

	return 0;
}

char *
parseNumber(uint8_t op)
{
	static char *cvt[] = {
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		".",
		"ᴇ",
		"-",
		"?",
		"?",
		"?",
	};

	static char tmp[32];
	char *p = tmp;

	uint8_t nxt;

	// Numbers are terminated by a 0x00 byte.  We already have the first
	// character, but we have to look it up.

	// Convert into a temporary buffer, because we have to do some fixups.
	p = stpcpy(p, cvt[op & 0x0f]);
	while((nxt = getNextByte()) != 0x00) {
		p = stpcpy(p, cvt[nxt & 0x0f]);
	}

	if(strcmp(tmp, "ᴇ") == 0) {
		strcpy(tmp, "1");
	}

	if(strcmp(tmp, "-ᴇ") == 0) {
		strcpy(tmp, "-1");
	}

	return tmp;
}

void
parseFn(uint8_t op)
{
	int i;

	uint8_t chr = getNextByte();

	if(!suppressor) {
		bufferAdd("%02d ", curLine++);
	}

	// Often, the length is encoded in the op byte, but sadly there are
	// exceptions...
	if(op == 0xf1) {
		switch(chr) {
			case 0xd5: bufferAdd("FIX 10");		return;
			case 0xd6: bufferAdd("SCI 10");		return;
			case 0xd7: bufferAdd("ENG 10");		return;
			case 0xe5: bufferAdd("FIX 11");		return;
			case 0xe6: bufferAdd("SCI 11");		return;
			case 0xe7: bufferAdd("ENG 11");		return;

			default: // Not special - fall through.
				break;
		}
	} else if(op == 0xf2) {
		uint8_t reg = getNextByte();

		switch(chr) {
			case 0xa0: bufferAdd("RTNERR %s",	decodeNN(reg, 1)); return;
			case 0xa1: bufferAdd("DROPN %s",	decodeNN(reg, 1)); return;
			case 0xa2: bufferAdd("DUPN %s",		decodeNN(reg, 1)); return;
			case 0xa3: bufferAdd("PICK %s",		decodeNN(reg, 1)); return;
			case 0xa4: bufferAdd("UNPICK %s",	decodeNN(reg, 1)); return;
			case 0xa5: bufferAdd("R↓N %s",		decodeNN(reg, 1)); return;
			case 0xa6: bufferAdd("R↑N %s",		decodeNN(reg, 1)); return;
			case 0xc8: bufferAdd("LASTO %s",	decodeNN(reg, 1)); return;
			case 0xd0: bufferAdd("INPUT %s",	decodeNN(reg, 2)); return;
			case 0xd1: bufferAdd("RCL+ %s",		decodeNN(reg, 2)); return;
			case 0xd2: bufferAdd("RCL- %s",		decodeNN(reg, 2)); return;
			case 0xd3: bufferAdd("RCL× %s",		decodeNN(reg, 2)); return;
			case 0xd4: bufferAdd("RCL÷ %s",		decodeNN(reg, 2)); return;
			case 0xd8: bufferAdd("CLV %s",		decodeNN(reg, 2)); return;
			case 0xd9: bufferAdd("PRV %s",		decodeNN(reg, 2)); return;
			case 0xda: bufferAdd("INDEX %s",	decodeNN(reg, 2)); return;
			case 0xe0: bufferAdd("FUNC %s",		decodeNN(reg, 2)); return;
			case 0xe8: bufferAdd("PGMINT %s",	decodeNN(reg, 2)); return;
			case 0xe9: bufferAdd("PGMSLV %s",	decodeNN(reg, 2)); return;
			case 0xea: bufferAdd("INTEG %s",	decodeNN(reg, 2)); return;
			case 0xeb: bufferAdd("SOLVE %s",	decodeNN(reg, 2)); return;
			case 0xec: bufferAdd("DIM %s",		decodeNN(reg, 2)); return;
			case 0xed: bufferAdd("LSTO %s",		decodeNN(reg, 2)); return;
			case 0xee: bufferAdd("INPUT %s",	decodeNN(reg, 2)); return;
			case 0xef: bufferAdd("EDITN %s",	decodeNN(reg, 2)); return;
			case 0xf8: bufferAdd("VARMENU %s",	decodeNN(reg, 2)); return;

			default: // Not special - push the byte back and fall through.
				pushBack();
				break;
		}
	} else if(op == 0xf3) {
		uint8_t b1 = getNextByte();
		uint8_t b2 = getNextByte();

		switch(chr) {
			case 0xa7:
				switch(b1) {
					case 0x08: bufferAdd("RENAME %s",	decodeNN(b2, 2)); return;
					case 0x10: bufferAdd("CHDIR %s",	decodeNN(b2, 2)); return;
					case 0x11: bufferAdd("XASTO %s",	decodeNN(b2, 2)); return;
					case 0x12: bufferAdd("LXASTO %s",	decodeNN(b2, 2)); return;

					case 0x13:
						if(b2 & 0x80) {
							bufferAdd("HEAD IND %s",decodeNN(b2 & 0x7f, 2)); return;
						} else {
							bufferAdd("HEAD %s",	decodeNN(b2 & 0x7f, 2)); return;
						}
						break;

					case 0x14: bufferAdd("X=? %s",		decodeNN(b2, 2)); return;
					case 0x15: bufferAdd("X≠? %s",		decodeNN(b2, 2)); return;
					case 0x16: bufferAdd("X<? %s",		decodeNN(b2, 2)); return;
					case 0x17: bufferAdd("X>? %s",		decodeNN(b2, 2)); return;
					case 0x18: bufferAdd("X≤? %s",		decodeNN(b2, 2)); return;
					case 0x19: bufferAdd("X≥? %s",		decodeNN(b2, 2)); return;
					case 0x1a: bufferAdd("0=? %s",		decodeNN(b2, 2)); return;
					case 0x1b: bufferAdd("0≠? %s",		decodeNN(b2, 2)); return;
					case 0x1c: bufferAdd("0<? %s",		decodeNN(b2, 2)); return;
					case 0x1d: bufferAdd("0>? %s",		decodeNN(b2, 2)); return;
					case 0x1e: bufferAdd("0≤? %s",		decodeNN(b2, 2)); return;
					case 0x1f: bufferAdd("0≥? %s",		decodeNN(b2, 2)); return;
					case 0x30: bufferAdd("PGMVAR IND %s",	decodeNN(b2 & 0x7f, 2)); return;
					case 0x31: bufferAdd("VARMNU1 IND %s",	decodeNN(b2 & 0x7f, 2)); return;
					case 0x36: bufferAdd("EVALN %s",	decodeNN(b2, 2)); return;
					case 0x37: bufferAdd("EQNSLV %s",	decodeNN(b2, 2)); return;
					case 0x38: bufferAdd("EQNINT %s",	decodeNN(b2, 2)); return;
					case 0x39: bufferAdd("EQNVAR %s",	decodeNN(b2, 2)); return;
					case 0x3a: bufferAdd("EQNMENU %s",	decodeNN(b2, 2)); return;
					case 0x3d: bufferAdd("RAISE %s",	decodeNN(b2, 1)); return;
					case 0x3e: bufferAdd("CRDIR %s",	decodeNN(b2, 2)); return;
					case 0x3f: bufferAdd("PGDIR %s",	decodeNN(b2, 2)); return;
					case 0x60: bufferAdd("PGMPLOT %s",	decodeNN(b2, 2)); return;
					case 0x61: bufferAdd("EQNPLOT %s",	decodeNN(b2, 2)); return;
					case 0x62: bufferAdd("XAXIS %s",	decodeNN(b2, 2)); return;
					case 0x63: bufferAdd("YAXIS %s",	decodeNN(b2, 2)); return;
					case 0x64: bufferAdd("LCLV %s",		decodeNN(b2, 2)); return;

					default:  // Not special - push the bytes back.
						pushBack();
						pushBack();
						break;
				}
				break;

			case 0xe2: bufferAdd("KEY %d XEQ %s", b1, decodeNN(b2, 2)); return;
			case 0xe3: bufferAdd("KEY %d GTO %s", b1, decodeNN(b2, 2)); return;
			case 0xf6: bufferAdd("DEL %02d", (b1 << 8) | b2); return;
			case 0xf7: bufferAdd("SIZE %02d", (b1 << 8) | b2); return;

			default: // Not special - push the bytes back and fall through.
				pushBack();
				pushBack();
				break;
		}
	} else if(op == 0xf4) {
		uint8_t b1 = getNextByte();
		uint8_t b2 = getNextByte();
		uint8_t b3 = getNextByte();

		switch(chr) {
			case 0xa7:
				switch(b1) {
					case 0x32: bufferAdd("GTOL %d", (b2 << 8) | b3); return;
					case 0x33: bufferAdd("XEQL %d", (b2 << 8) | b3); return;
					case 0x41: bufferAdd("_%c%c", b2, b3); suppressor = false; return;

					default: // Not special - push the bytes back and fall through.
						pushBack();
						pushBack();
						break;
				}

			default: // Not special - push the bytes back and fall through.
				pushBack();
				pushBack();
				pushBack();
				break;
		}
	}

	int len = (op & 0x0f) - 1;
	bool extraNum = false;
	uint8_t b1;

	switch(chr) {
		case 0x7f: bufferAdd("%s\"", translate(chr)); break;
		case 0x80: bufferAdd("VIEW \""); break;
		case 0x81: bufferAdd("STO \""); break;
		case 0x82: bufferAdd("STO+ \""); break;
		case 0x83: bufferAdd("STO- \""); break;
		case 0x84: bufferAdd("STO× \""); break;
		case 0x85: bufferAdd("STO÷ \""); break;
		case 0x86: bufferAdd("X<> \""); break;
		case 0x87: bufferAdd("INDEX \""); break;
		case 0x88: bufferAdd("VIEW IND \""); break;
		case 0x89: bufferAdd("STO IND \""); break;
		case 0x8a: bufferAdd("STO+ IND \""); break;
		case 0x8b: bufferAdd("STO- IND \""); break;
		case 0x8c: bufferAdd("STO× IND \""); break;
		case 0x8d: bufferAdd("STO÷ IND \""); break;
		case 0x8e: bufferAdd("X<> IND \""); break;
		case 0x8f: bufferAdd("INDEX IND \""); break;
		case 0x90: bufferAdd("MVAR \""); break;
		case 0x91: bufferAdd("RCL \""); break;
		case 0x92: bufferAdd("RCL+ \""); break;
		case 0x93: bufferAdd("RCL- \""); break;
		case 0x94: bufferAdd("RCL× \""); break;
		case 0x95: bufferAdd("RCL÷ \""); break;
		case 0x96: bufferAdd("ISG \""); break;
		case 0x97: bufferAdd("DSE \""); break;
		case 0x99: bufferAdd("RCL IND \""); break;
		case 0x9a: bufferAdd("RCL+ IND \""); break;
		case 0x9b: bufferAdd("RCL- IND \""); break;
		case 0x9c: bufferAdd("RCL× IND \""); break;
		case 0x9d: bufferAdd("RCL÷ IND \""); break;
		case 0x9e: bufferAdd("ISG IND \""); break;
		case 0x9f: bufferAdd("DSE IND \""); break;

		case 0xa7: 
			   b1 = getNextByte();
			   --len;

			   switch(b1) {
				   case 0x01: bufferAdd("XASTO \""); break;
				   case 0x02: bufferAdd("LXASTO \""); break;
				   case 0x03: bufferAdd("HEAD \""); break;
				   case 0x04: bufferAdd("X=? \""); break;
				   case 0x05: bufferAdd("X≠? \""); break;
				   case 0x06: bufferAdd("X<? \""); break;
				   case 0x07: bufferAdd("X>? \""); break;
				   case 0x09: bufferAdd("XASTO IND \""); break;
				   case 0x0a: bufferAdd("LXASTO IND \""); break;
				   case 0x0b: bufferAdd("HEAD IND \""); break;
				   case 0x0c: bufferAdd("X=? IND \""); break;
				   case 0x0d: bufferAdd("X≠? IND \""); break;
				   case 0x0e: bufferAdd("X<? IND \""); break;
				   case 0x0f: bufferAdd("X>? IND \""); break;
				   case 0x22: bufferAdd("0=? \""); break;
				   case 0x23: bufferAdd("0≠? \""); break;
				   case 0x24: bufferAdd("0<? \""); break;
				   case 0x25: bufferAdd("0>? \""); break;
				   case 0x20: bufferAdd("X≤? \""); break;
				   case 0x21: bufferAdd("X≥? \""); break;
				   case 0x26: bufferAdd("0≤? \""); break;
				   case 0x27: bufferAdd("0≥? \""); break;
				   case 0x28: bufferAdd("X≤? IND \""); break;
				   case 0x29: bufferAdd("X≥? IND \""); break;
				   case 0x2a: bufferAdd("0=? IND \""); break;
				   case 0x2b: bufferAdd("0≠? IND \""); break;
				   case 0x2c: bufferAdd("0<? IND \""); break;
				   case 0x2d: bufferAdd("0>? IND \""); break;
				   case 0x2e: bufferAdd("0≤? IND \""); break;
				   case 0x2f: bufferAdd("0≥? IND \""); break;
				   case 0x34: bufferAdd("GSTO \""); break;
				   case 0x35: bufferAdd("GRCL \""); break;
				   case 0x3c: bufferAdd("RAISE IND \""); break;
				   case 0x40: bufferAdd("PGMVAR \""); break;
				   case 0x42: bufferAdd("VARMNU1 \""); break;
				   case 0x43: bufferAdd("EVALN \""); break;
				   case 0x44: bufferAdd("EQNSLV \""); break;
				   case 0x45: bufferAdd("EQNINT \""); break;
				   case 0x46: bufferAdd("EQNVAR \""); break;
				   case 0x47: bufferAdd("EQNMENU \""); break;
				   case 0x48: bufferAdd("PGMVAR IND \""); break;
				   case 0x4a: bufferAdd("VARMNU1 IND \""); break;
				   case 0x4b: bufferAdd("EVALN IND \""); break;
				   case 0x4c: bufferAdd("EQNSLV IND \""); break;
				   case 0x4d: bufferAdd("EQNINT IND \""); break;
				   case 0x4e: bufferAdd("EQNVAR IND \""); break;
				   case 0x4f: bufferAdd("EQNMENU IND \""); break;

				   case 0x41: 
					      if(!suppressor) {
						      bufferAdd("XSTR \"");
					      }
					      suppressor = false;
					      break;

				   case 0x49:
					      if(!suppressor) {
						      bufferAdd("XSTR \"");
					      }
					      suppressor = true;
					      break;

				   case 0x51: bufferAdd("CRDIR \""); break;
				   case 0x52: bufferAdd("PGDIR \""); break;
				   case 0x53: bufferAdd("CHDIR \""); break;
				   case 0x54: bufferAdd("RENAME \""); break;
				   case 0x55: bufferAdd("PGMPLOT \""); break;
				   case 0x56: bufferAdd("EQNPLOT \""); break;
				   case 0x57: bufferAdd("XAXIS \""); break;
				   case 0x59: bufferAdd("CRDIR IND \""); break;
				   case 0x5a: bufferAdd("PGDIR IND \""); break;
				   case 0x5b: bufferAdd("CHDIR IND \""); break;
				   case 0x5c: bufferAdd("RENAME IND \""); break;
				   case 0x5d: bufferAdd("PGMPLOT IND \""); break;
				   case 0x5e: bufferAdd("EQNPLOT IND \""); break;
				   case 0x5f: bufferAdd("XAXIS IND \""); break;
				   case 0x70: bufferAdd("YAXIS \""); break;
				   case 0x71: bufferAdd("LCLV \""); break;
				   case 0x78: bufferAdd("YAXIS IND \""); break;
				   case 0x79: bufferAdd("LCLV IND \""); break;
				   default:   break;
			   }
			   break;

		case 0xa8: bufferAdd("SF IND \""); break;
		case 0xa9: bufferAdd("CF IND \""); break;
		case 0xaa: bufferAdd("FS?C IND \""); break;
		case 0xab: bufferAdd("FC?C IND \""); break;
		case 0xac: bufferAdd("FS? IND \""); break;
		case 0xad: bufferAdd("FC? IND \""); break;
		case 0xae: bufferAdd("GTO IND \""); break;
		case 0xaf: bufferAdd("XEQ IND \""); break;
		case 0xb0: bufferAdd("CLV \""); break;
		case 0xb1: bufferAdd("PRV \""); break;
		case 0xb2: bufferAdd("ASTO \""); break;
		case 0xb3: bufferAdd("ARCL \""); break;
		case 0xb4: bufferAdd("PGMINT \""); break;
		case 0xb5: bufferAdd("PGMSLV \""); break;
		case 0xb6: bufferAdd("INTEG \""); break;
		case 0xb7: bufferAdd("SOLVE \""); break;
		case 0xb8: bufferAdd("CLV IND \""); break;
		case 0xb9: bufferAdd("PRV IND \""); break;
		case 0xba: bufferAdd("ASTO IND \""); break;
		case 0xbb: bufferAdd("ARCL IND \""); break;
		case 0xbc: bufferAdd("PGMINT IND \""); break;
		case 0xbd: bufferAdd("PGMSLV IND \""); break;
		case 0xbe: bufferAdd("INTEG IND \""); break;
		case 0xbf: bufferAdd("SOLVE IND \""); break;
		case 0xc0: bufferAdd("ASSIGN \""); extraNum = true; --len; break;
		case 0xc1: bufferAdd("VARMENU \""); break;
		case 0xc2: bufferAdd("KEY %d XEQ \"", getNextByte()); --len; break;
		case 0xc3: bufferAdd("KEY %d GTO \"", getNextByte()); --len; break;
		case 0xc4: bufferAdd("DIM \""); break;
		case 0xc5: bufferAdd("INPUT \""); break;
		case 0xc6: bufferAdd("EDITN \""); break;
		case 0xc7: bufferAdd("LSTO \""); break;
		case 0xc9: bufferAdd("VARMENU IND \""); break;
		case 0xca: bufferAdd("KEY %d XEQ IND \"", getNextByte()); --len; break;
		case 0xcb: bufferAdd("KEY %d GTO IND \"", getNextByte()); --len; break;
		case 0xcc: bufferAdd("DIM IND \""); break;
		case 0xcd: bufferAdd("INPUT IND \""); break;
		case 0xce: bufferAdd("EDITN IND \""); break;
		case 0xcf: bufferAdd("LSTO IND \""); break;
		case 0xdb: bufferAdd("ΣREG IND \""); break;
		case 0xdc: bufferAdd("FIX IND \""); break;
		case 0xdd: bufferAdd("SCI IND \""); break;
		case 0xde: bufferAdd("ENG IND \""); break;
		case 0xdf: bufferAdd("TONE IND \""); break;
		case 0xe4: bufferAdd("RTNERR IND \""); break;
		case 0xf0: bufferAdd("CLP \""); break;
		case 0xf5: bufferAdd("LASTO \""); break;
		case 0xf9: bufferAdd("DROPN IND \""); break;
		case 0xfa: bufferAdd("DUPN IND \""); break;
		case 0xfb: bufferAdd("PICK IND \""); break;
		case 0xfc: bufferAdd("UNPICK IND \""); break;
		case 0xfd: bufferAdd("LASTO IND \""); break;
		case 0xfe: bufferAdd("R↓N IND \""); break;
		case 0xff: bufferAdd("R↑N IND \""); break;
		default:   bufferAdd("\"%s", translate(chr)); break;
	}

	for(i = 0; i < len; i++) {
		bufferAdd("%s", translate(getNextByte()));
	}

	if(!suppressor) {
		bufferAdd("\"");
	}

	if(extraNum) {
		uint8_t which = getNextByte();

		if(which <= 0x11) {
			bufferAdd(" TO %02d", which + 1);
		}
	}
}

void
parse1E(uint8_t op)
{
	int len = getNextByte() & 0x0f;
	int i;

	switch(op) {
		case 0x1d:
			bufferAdd("%02d GTO \"", curLine++);
			break;

		case 0x1e:
			bufferAdd("%02d XEQ \"", curLine++);
			break;

		default:
			bufferAdd("%02d ??? \"", curLine++);
			break;
	}

	for(i = 0; i < len; i++) {
		bufferAdd("%s", translate(getNextByte()));
	}
	bufferAdd("\"");
}

void
parse90(uint8_t op)
{
	bufferAdd("%02d ", curLine++);

	switch(op) {
		case 0x90: bufferAdd("RCL ");		break;
		case 0x91: bufferAdd("STO ");		break;
		case 0x92: bufferAdd("STO+ ");		break;
		case 0x93: bufferAdd("STO- ");		break;
		case 0x94: bufferAdd("STO× ");		break;
		case 0x95: bufferAdd("STO÷ ");		break;
		case 0x96: bufferAdd("ISG ");		break;
		case 0x97: bufferAdd("DSE ");		break;
		case 0x98: bufferAdd("VIEW ");		break;
		case 0x99: bufferAdd("ΣREG ");		break;
		case 0x9a: bufferAdd("ASTO ");		break;
		case 0x9b: bufferAdd("ARCL ");		break;
		case 0x9c: bufferAdd("FIX ");		break;
		case 0x9d: bufferAdd("SCI ");		break;
		case 0x9e: bufferAdd("ENG ");		break;
		default:   bufferAdd("???");		break;
	}

	bufferAdd("%s", decodeNN(getNextByte(), 2));
}

void
parseA0(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d ", curLine++);

	switch(chr) {
		case 0x61: bufferAdd("SINH");		break;
		case 0x62: bufferAdd("COSH");		break;
		case 0x63: bufferAdd("TANH");		break;
		case 0x64: bufferAdd("ASINH");		break;
		case 0x65: bufferAdd("ATANH");		break;
		case 0x66: bufferAdd("ACOSH");		break;
		case 0x6f: bufferAdd("COMB");		break;
		case 0x70: bufferAdd("PERM");		break;
		case 0x71: bufferAdd("RAN");		break;
		case 0x72: bufferAdd("COMPLEX");	break;
		case 0x73: bufferAdd("SEED");		break;
		case 0x74: bufferAdd("GAMMA");		break;
		case 0x9f: bufferAdd("BEST");		break;
		case 0xa0: bufferAdd("EXPF");		break;
		case 0xa1: bufferAdd("LINF");		break;
		case 0xa2: bufferAdd("LOGF");		break;
		case 0xa3: bufferAdd("PWRF");		break;
		case 0xa4: bufferAdd("SLOPE");		break;
		case 0xa5: bufferAdd("SUM");		break;
		case 0xa6: bufferAdd("YINT");		break;
		case 0xa7: bufferAdd("CORR");		break;
		case 0xa8: bufferAdd("FCSTX");		break;
		case 0xa9: bufferAdd("FCSTY");		break;
		case 0xaa: bufferAdd("INSR");		break;
		case 0xab: bufferAdd("DELR");		break;
		case 0xac: bufferAdd("WMEAN");		break;
		case 0xad: bufferAdd("LINΣ");		break;
		case 0xae: bufferAdd("ALLΣ");		break;
		case 0xe2: bufferAdd("HEXM");		break;
		case 0xe3: bufferAdd("DECM");		break;
		case 0xe4: bufferAdd("OCTM");		break;
		case 0xe5: bufferAdd("BINM");		break;
		case 0xe6: bufferAdd("BASE+");		break;
		case 0xe7: bufferAdd("BASE-");		break;
		case 0xe8: bufferAdd("BASE×");		break;
		case 0xe9: bufferAdd("BASE÷");		break;
		case 0xea: bufferAdd("BASE+/-");	break;
		default:   bufferAdd("XROM ???");	break;
	}
}

void
parseA2(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d ", curLine++);

	switch(chr) {
		case 0x59: bufferAdd("POLAR");		break;
		case 0x5a: bufferAdd("RECT");		break;
		case 0x5b: bufferAdd("RDX.");		break;
		case 0x5c: bufferAdd("RDX,");		break;
		case 0x5d: bufferAdd("ALL");		break;
		case 0x5e: bufferAdd("MENU");		break;
		case 0x5f: bufferAdd("X≥0?");		break;
		case 0x60: bufferAdd("X≥Y?");		break;
		case 0x61: bufferAdd("CLALLb");		break;
		case 0x62: bufferAdd("CLKEYS");		break;
		case 0x63: bufferAdd("KEYASN");		break;
		case 0x64: bufferAdd("LCLBL");		break;
		case 0x65: bufferAdd("REAL?");		break;
		case 0x66: bufferAdd("MAT?");		break;
		case 0x67: bufferAdd("CPX?");		break;
		case 0x68: bufferAdd("STR?");		break;
		case 0x69: bufferAdd("QUIET");		break;
		case 0x6a: bufferAdd("CPXRES");		break;
		case 0x6b: bufferAdd("REALRES");	break;
		case 0x6c: bufferAdd("EXITALL");	break;
		case 0x6d: bufferAdd("CLMENU");		break;
		case 0x6e: bufferAdd("GETKEY");		break;
		case 0x6f: bufferAdd("CUSTOM");		break;
		case 0x70: bufferAdd("ON");		break;
		case 0x71: bufferAdd("DROP");		break;
		case 0x72: bufferAdd("WIDTH");		break;
		case 0x73: bufferAdd("HEIGHT");		break;
		case 0x74: bufferAdd("SKIP");		break;
		case 0x75: bufferAdd("CPXMAT?");	break;
		case 0x76: bufferAdd("TYPE?");		break;
		case 0x77: bufferAdd("N");		break;
		case 0x78: bufferAdd("I%%YR");		break;
		case 0x79: bufferAdd("PV");		break;
		case 0x7a: bufferAdd("PMT");		break;
		case 0x7b: bufferAdd("FV");		break;
		case 0x7c: bufferAdd("GETDS");		break;
		case 0x7d: bufferAdd("SETDS");		break;
		case 0x7e: bufferAdd("DIRECT");		break;
		case 0x7f: bufferAdd("NUMERIC");	break;
		default:   bufferAdd("XROM ???");	break;
	}
}

void
parseA5(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d ", curLine++);

	switch(chr) {
		case 0x87: bufferAdd("NOT");		break;
		case 0x88: bufferAdd("AND");		break;
		case 0x89: bufferAdd("OR");		break;
		case 0x8a: bufferAdd("XOR");		break;
		case 0x8b: bufferAdd("ROTXY");		break;
		case 0x8c: bufferAdd("BIT?");		break;
		default:   bufferAdd("XROM ???");	break;
	}
}
 
void
parseA6(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d ", curLine++);

	switch(chr) {
		case 0x31: bufferAdd("AIP");		break;
		case 0x41: bufferAdd("ALENG");		break;
		case 0x42: bufferAdd("ANUM");		break;
		case 0x46: bufferAdd("AROT");		break;
		case 0x47: bufferAdd("ATOX");		break;
		case 0x5c: bufferAdd("POSA");		break;
		case 0x60: bufferAdd("RCLFLAG");	break;
		case 0x6d: bufferAdd("STOFLAG");	break;
		case 0x6e: bufferAdd("X<>F");		break;
		case 0x6f: bufferAdd("XTOA");		break;
		case 0x78: bufferAdd("ΣREG?");		break;
		case 0x81: bufferAdd("ADATE");		break;
		case 0x84: bufferAdd("ATIME");		break;
		case 0x85: bufferAdd("ATIME24");	break;
		case 0x86: bufferAdd("CLK12");		break;
		case 0x87: bufferAdd("CLK24");		break;
		case 0x8c: bufferAdd("DATE");		break;
		case 0x8d: bufferAdd("DATE+");		break;
		case 0x8e: bufferAdd("DDAYS");		break;
		case 0x8f: bufferAdd("DMY");		break;
		case 0x90: bufferAdd("DOW");		break;
		case 0x91: bufferAdd("MDY");		break;
		case 0x9c: bufferAdd("TIME");		break;
		case 0xc9: bufferAdd("TRANS");		break;
		case 0xca: bufferAdd("CROSS");		break;
		case 0xcb: bufferAdd("DOT");		break;
		case 0xcc: bufferAdd("DET");		break;
		case 0xcd: bufferAdd("UVEC");		break;
		case 0xce: bufferAdd("INVRT");		break;
		case 0xcf: bufferAdd("FNRM");		break;
		case 0xd0: bufferAdd("RSUM");		break;
		case 0xd1: bufferAdd("R<>R");		break;
		case 0xd2: bufferAdd("I+");		break;
		case 0xd3: bufferAdd("I-");		break;
		case 0xd4: bufferAdd("J+");		break;
		case 0xd5: bufferAdd("J-");		break;
		case 0xd6: bufferAdd("STOEL");		break;
		case 0xd7: bufferAdd("RCLEL");		break;
		case 0xd8: bufferAdd("STOIJ");		break;
		case 0xd9: bufferAdd("RCLIJ");		break;
		case 0xda: bufferAdd("NEWMAT");		break;
		case 0xdb: bufferAdd("OLD");		break;
		case 0xdc: bufferAdd("←");		break;
		case 0xdd: bufferAdd("→");		break;
		case 0xde: bufferAdd("↑");		break;
		case 0xdf: bufferAdd("↓");		break;
		case 0xe0: bufferAdd("GOTOROW");	break;
		case 0xe1: bufferAdd("EDIT");		break;
		case 0xe2: bufferAdd("WRAP");		break;
		case 0xe3: bufferAdd("GROW");		break;
		case 0xe4: bufferAdd("MATA");		break;
		case 0xe5: bufferAdd("MATB");		break;
		case 0xe6: bufferAdd("MATX");		break;
		case 0xe7: bufferAdd("DIM?");		break;
		case 0xe8: bufferAdd("GETM");		break;
		case 0xe9: bufferAdd("PUTM");		break;
		case 0xea: bufferAdd("[MIN]");		break;
		case 0xeb: bufferAdd("[MAX]");		break;
		case 0xec: bufferAdd("[FIND]");		break;
		case 0xed: bufferAdd("RNRM");		break;
		case 0xef: bufferAdd("PRALL");		break;
		case 0xf0: bufferAdd("$FV");		break;
		case 0xf1: bufferAdd("CONVERT");	break;
		case 0xf2: bufferAdd("UBASE");		break;
		case 0xf3: bufferAdd("UVAL");		break;
		case 0xf4: bufferAdd("UFACT");		break;
		case 0xf5: bufferAdd("→UNIT");		break;
		case 0xf6: bufferAdd("UNIT→");		break;

		case 0xf7:
			   bufferAdd("%s", parseNumber(getNextByte()));
			   extraLen = bufferLen();
			   suppressor = true;
			   break;

		case 0xf8: bufferAdd("UNIT?");		break;
		case 0xf9: bufferAdd("UPDIR");		break;
		case 0xfa: bufferAdd("HOME");		break;
		case 0xfb: bufferAdd("PATH");		break;
		case 0xfc: bufferAdd("→LIST");		break;
		case 0xfd: bufferAdd("LIST→");		break;
		default:   bufferAdd("XROM ???");	break;
	}
}

void
parseA7(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d ", curLine++);

	switch(chr) {
		case 0x19: bufferAdd("XVIEW");		break;
		case 0x1a: bufferAdd("PLOT");		break;
		case 0x1b: bufferAdd("A...F");		break;
		case 0x1c: bufferAdd("FSTART");		break;
		case 0x1d: bufferAdd("NN→S");		break;
		case 0x1e: bufferAdd("XMIN");		break;
		case 0x1f: bufferAdd("XMAX");		break;
		case 0x20: bufferAdd("YMIN");		break;
		case 0x21: bufferAdd("YMAX");		break;
		case 0x22: bufferAdd("SCAN");		break;
		case 0x23: bufferAdd("LINE");		break;
		case 0x24: bufferAdd("LIFE");		break;
		case 0x47: bufferAdd("LIST");		break;
		case 0x48: bufferAdd("PRA");		break;
		case 0x4d: bufferAdd("PRP");		break;
		case 0x50: bufferAdd("PRREG");		break;
		case 0x52: bufferAdd("PRΣ");		break;
		case 0x53: bufferAdd("PRSTK");		break;
		case 0x54: bufferAdd("PRX");		break;
		case 0x5b: bufferAdd("MAN");		break;
		case 0x5c: bufferAdd("NORM");		break;
		case 0x5d: bufferAdd("TRACE");		break;
		case 0x5e: bufferAdd("PRON");		break;
		case 0x5f: bufferAdd("PROFF");		break;
		case 0x60: bufferAdd("DELAY");		break;
		case 0x61: bufferAdd("PRUSR");		break;
		case 0x62: bufferAdd("PRLCD");		break;
		case 0x63: bufferAdd("CLLCD");		break;
		case 0x64: bufferAdd("AGRAPH");		break;
		case 0x65: bufferAdd("PIXEL");		break;
		case 0xa5: bufferAdd("STD");		break;
		case 0xa6: bufferAdd("COMP");		break;
		case 0xa7: bufferAdd("SVAR");		break;
		case 0xa8: bufferAdd("GETITEM");	break;
		case 0xa9: bufferAdd("=");		break;
		case 0xaa: bufferAdd("≠");		break;
		case 0xab: bufferAdd("<");		break;
		case 0xac: bufferAdd(">");		break;
		case 0xad: bufferAdd("≤");		break;
		case 0xae: bufferAdd("≥");		break;
		case 0xaf: bufferAdd("&&");		break;
		case 0xb0: bufferAdd("||");		break;
		case 0xb1: bufferAdd("^^");		break;
		case 0xb2: bufferAdd("!");		break;
		case 0xb3: bufferAdd("IF?");		break;
		case 0xb4: bufferAdd("TRUNC");		break;
		case 0xb5: bufferAdd("DDAYSC");		break;
		case 0xb6: bufferAdd("GETEQN");		break;
		case 0xb7: bufferAdd("→PAR");		break;
		case 0xb8: bufferAdd("FSTACK");		break;
		case 0xb9: bufferAdd("PUTITEM");	break;
		case 0xc0: bufferAdd("$PMT");		break;
		case 0xba: bufferAdd("ΣN");		break;
		case 0xbb: bufferAdd("ΣX");		break;
		case 0xbc: bufferAdd("ΣX2");		break;
		case 0xbd: bufferAdd("ΣY");		break;
		case 0xbe: bufferAdd("ΣY2");		break;
		case 0xbf: bufferAdd("ΣXY");		break;
		case 0xc1: bufferAdd("ΣLNX");		break;
		case 0xc2: bufferAdd("ΣLNX2");		break;
		case 0xc3: bufferAdd("ΣLNY");		break;
		case 0xc4: bufferAdd("ΣLNY2");		break;
		case 0xc5: bufferAdd("ΣLNXLNY");	break;
		case 0xc6: bufferAdd("ΣXLNY");		break;
		case 0xc7: bufferAdd("ΣYLNX");		break;
		case 0xc8: bufferAdd("SPPV");		break;
		case 0xc9: bufferAdd("SPFV");		break;
		case 0xca: bufferAdd("USPV");		break;
		case 0xcc: bufferAdd("$N");		break;
		case 0xcb: bufferAdd("USFV");		break;
		case 0xcd: bufferAdd("$I%%YR");		break;
		case 0xce: bufferAdd("$PV");		break;
		case 0xcf: bufferAdd("ACCEL");		break;
		case 0xd0: bufferAdd("LOCAT");		break;
		case 0xd1: bufferAdd("HEADING");	break;
		case 0xd2: bufferAdd("FPTEST");		break;
		case 0xd3: bufferAdd("WSIZE");		break;
		case 0xd4: bufferAdd("WSIZE?");		break;
		case 0xd5: bufferAdd("YMD");		break;
		case 0xd6: bufferAdd("BSIGNED");	break;
		case 0xd7: bufferAdd("BWRAP");		break;
		case 0xd8: bufferAdd("BRESET");		break;
		case 0xd9: bufferAdd("GETKEY1");	break;
		case 0xda: bufferAdd("FMA");		break;
		case 0xdb: bufferAdd("CSLD?");		break;
		case 0xde: bufferAdd("RTNYES");		break;
		case 0xdf: bufferAdd("RTNNO");		break;
		case 0xe1: bufferAdd("STRACE");		break;
		case 0xe2: bufferAdd("4STK");		break;
		case 0xe3: bufferAdd("L4STK");		break;
		case 0xe4: bufferAdd("NSTK");		break;
		case 0xe5: bufferAdd("LNSTK");		break;
		case 0xe6: bufferAdd("DEPTH");		break;
		case 0xe7: bufferAdd("DUP");		break;
		case 0xe9: bufferAdd("APPEND");		break;
		case 0xe8: bufferAdd("PGMMENU");	break;
		case 0xea: bufferAdd("EXTEND");		break;
		case 0xeb: bufferAdd("SUBSTR");		break;
		case 0xec: bufferAdd("LENGTH");		break;
		case 0xed: bufferAdd("REV");		break;
		case 0xee: bufferAdd("POS");		break;
		case 0xef: bufferAdd("S→N");		break;
		case 0xf0: bufferAdd("N→S");		break;
		case 0xf1: bufferAdd("C→N");		break;
		case 0xf2: bufferAdd("N→C");		break;
		case 0xf3: bufferAdd("LIST?");		break;
		case 0xf4: bufferAdd("NEWLIST");	break;
		case 0xf6: bufferAdd("ERRMSG");		break;
		case 0xf7: bufferAdd("ERRNO");		break;
		case 0xf8: bufferAdd("RCOMPLX");	break;
		case 0xf9: bufferAdd("PCOMPLX");	break;
		case 0xfc: bufferAdd("PARSE");		break;
		case 0xfd: bufferAdd("UNPARSE");	break;
		case 0xfe: bufferAdd("EVAL");		break;
		case 0xff: bufferAdd("EQN?");		break;
		default:   bufferAdd("XROM ???");	break;
	}
}

void
parseFlags(uint8_t op)
{
	uint8_t chr = getNextByte();

	bufferAdd("%02d ", curLine++);

	switch(op) {
		case 0xa8: bufferAdd("SF ");		break;
		case 0xa9: bufferAdd("CF ");		break;
		case 0xaa: bufferAdd("FS?C ");		break;
		case 0xab: bufferAdd("FC?C ");		break;
		case 0xac: bufferAdd("FS? ");		break;
		case 0xad: bufferAdd("FC? ");		break;
		default:   bufferAdd("???");		break;
	}

	bufferAdd("%s", decodeNN(chr, 2));
}

void
process(int pass, int programLength)
{
	uint8_t op;
	size_t savedPosition = 0;
	bool complete = false;

	if(pass == SECOND_PASS) {
		printf("%02d { %d-Byte Prgm }\n", curLine++, programLength);
	}

	// This could be a while(1) loop, but in case there is a missing
	// END statement, it is better to avoid running off the end of
	// the file.
	while(sizeRemaining() > 0) {
		if(!suppressor) {
			savedPosition = getPosition();
		}

		switch((op = getNextByte())) {
			case 0x00:
				bufferAdd("%02d NULL", curLine++);
				break;

			case 0x01: case 0x02: case 0x03: case 0x04: case 0x05:
			case 0x06: case 0x07: case 0x08: case 0x09: case 0x0a:
			case 0x0b: case 0x0c: case 0x0d: case 0x0e: case 0x0f:
				bufferAdd("%02d%sLBL %02d", curLine++, GOOSE, op - 1);
				break;

			case 0x10: case 0x11: case 0x12: case 0x13: case 0x14:
			case 0x15: case 0x16: case 0x17: case 0x18: case 0x19:
			case 0x1a: case 0x1b: case 0x1c:
				bufferAdd("%02d %s", curLine++, parseNumber(op));
				break;

			case 0x1d: case 0x1e:
				parse1E(op);
				break;

			case 0x1f:
				bufferAdd("%02d 1f ???", curLine++);
				break;

			case 0x20: case 0x21: case 0x22: case 0x23: case 0x24:
			case 0x25: case 0x26: case 0x27: case 0x28: case 0x29:
			case 0x2a: case 0x2b: case 0x2c: case 0x2d: case 0x2e:
			case 0x2f:
				bufferAdd("%02d RCL %02d", curLine++, op & 0x0f);
				break;

			case 0x30: case 0x31: case 0x32: case 0x33: case 0x34:
			case 0x35: case 0x36: case 0x37: case 0x38: case 0x39:
			case 0x3a: case 0x3b: case 0x3c: case 0x3d: case 0x3e:
			case 0x3f:
				bufferAdd("%02d STO %02d", curLine++, op & 0x0f);
				break;

			case 0x40: bufferAdd("%02d +", curLine++); break;
			case 0x41: bufferAdd("%02d -", curLine++); break;
			case 0x42: bufferAdd("%02d ×", curLine++); break;
			case 0x43: bufferAdd("%02d ÷", curLine++); break;
			case 0x44: bufferAdd("%02d X<Y?", curLine++); break;
			case 0x45: bufferAdd("%02d X>Y?", curLine++); break;
			case 0x46: bufferAdd("%02d X≤Y?", curLine++); break;
			case 0x47: bufferAdd("%02d Σ+", curLine++); break;
			case 0x48: bufferAdd("%02d Σ-", curLine++); break;
			case 0x49: bufferAdd("%02d HMS+", curLine++); break;
			case 0x4a: bufferAdd("%02d HMS-", curLine++); break;
			case 0x4b: bufferAdd("%02d MOD", curLine++); break;
			case 0x4c: bufferAdd("%02d %%", curLine++); break;
			case 0x4d: bufferAdd("%02d %%CH", curLine++); break;
			case 0x4e: bufferAdd("%02d →REC", curLine++); break;
			case 0x4f: bufferAdd("%02d →POL", curLine++); break;
			case 0x50: bufferAdd("%02d LN", curLine++); break;
			case 0x51: bufferAdd("%02d X↑2", curLine++); break;
			case 0x52: bufferAdd("%02d SQRT", curLine++); break;
			case 0x53: bufferAdd("%02d Y↑X", curLine++); break;
			case 0x54: bufferAdd("%02d +/-", curLine++); break;
			case 0x55: bufferAdd("%02d E↑X", curLine++); break;
			case 0x56: bufferAdd("%02d LOG", curLine++); break;
			case 0x57: bufferAdd("%02d 10↑X", curLine++); break;
			case 0x58: bufferAdd("%02d E↑X-1", curLine++); break;
			case 0x59: bufferAdd("%02d SIN", curLine++); break;
			case 0x5a: bufferAdd("%02d COS", curLine++); break;
			case 0x5b: bufferAdd("%02d TAN", curLine++); break;
			case 0x5c: bufferAdd("%02d ASIN", curLine++); break;
			case 0x5d: bufferAdd("%02d ACOS", curLine++); break;
			case 0x5e: bufferAdd("%02d ATAN", curLine++); break;
			case 0x5f: bufferAdd("%02d →DEC", curLine++); break;
			case 0x60: bufferAdd("%02d 1/X", curLine++); break;
			case 0x61: bufferAdd("%02d ABS", curLine++); break;
			case 0x62: bufferAdd("%02d N!", curLine++); break;
			case 0x63: bufferAdd("%02d X≠0?", curLine++); break;
			case 0x64: bufferAdd("%02d X>0?", curLine++); break;
			case 0x65: bufferAdd("%02d LN1+X", curLine++); break;
			case 0x66: bufferAdd("%02d X<0?", curLine++); break;
			case 0x67: bufferAdd("%02d X=0?", curLine++); break;
			case 0x68: bufferAdd("%02d IP", curLine++); break;
			case 0x69: bufferAdd("%02d FP", curLine++); break;
			case 0x6a: bufferAdd("%02d →RAD", curLine++); break;
			case 0x6b: bufferAdd("%02d →DEG", curLine++); break;
			case 0x6c: bufferAdd("%02d →HMS", curLine++); break;
			case 0x6d: bufferAdd("%02d →HR", curLine++); break;
			case 0x6e: bufferAdd("%02d RND", curLine++); break;
			case 0x6f: bufferAdd("%02d →OCT", curLine++); break;
			case 0x70: bufferAdd("%02d CLΣ", curLine++); break;
			case 0x71: bufferAdd("%02d X<>Y", curLine++); break;
			case 0x72: bufferAdd("%02d PI", curLine++); break;
			case 0x73: bufferAdd("%02d CLST", curLine++); break;
			case 0x74: bufferAdd("%02d R↑", curLine++); break;
			case 0x75: bufferAdd("%02d R↓", curLine++); break;
			case 0x76: bufferAdd("%02d LASTX", curLine++); break;
			case 0x77: bufferAdd("%02d CLX", curLine++); break;
			case 0x78: bufferAdd("%02d X=Y?", curLine++); break;
			case 0x79: bufferAdd("%02d X≠Y?", curLine++); break;
			case 0x7a: bufferAdd("%02d SIGN", curLine++); break;
			case 0x7b: bufferAdd("%02d X≤0?", curLine++); break;
			case 0x7c: bufferAdd("%02d MEAN", curLine++); break;
			case 0x7d: bufferAdd("%02d SDEV", curLine++); break;
			case 0x7e: bufferAdd("%02d AVIEW", curLine++); break;
			case 0x7f: bufferAdd("%02d CLD", curLine++); break;
			case 0x80: bufferAdd("%02d DEG", curLine++); break;
			case 0x81: bufferAdd("%02d RAD", curLine++); break;
			case 0x82: bufferAdd("%02d GRAD", curLine++); break;
			case 0x83: bufferAdd("%02d ENTER", curLine++); break;
			case 0x84: bufferAdd("%02d STOP", curLine++); break;
			case 0x85: bufferAdd("%02d RTN", curLine++); break;
			case 0x86: bufferAdd("%02d BEEP", curLine++); break;
			case 0x87: bufferAdd("%02d CLA", curLine++); break;
			case 0x88: bufferAdd("%02d ASHF", curLine++); break;
			case 0x89: bufferAdd("%02d PSE", curLine++); break;
			case 0x8a: bufferAdd("%02d CLRG", curLine++); break;
			case 0x8b: bufferAdd("%02d AOFF", curLine++); break;
			case 0x8c: bufferAdd("%02d AON", curLine++); break;
			case 0x8d: bufferAdd("%02d OFF", curLine++); break;
			case 0x8e: bufferAdd("%02d PROMPT", curLine++); break;
			case 0x8f: bufferAdd("%02d ADV", curLine++); break;

			case 0x90: case 0x91: case 0x92: case 0x93: case 0x94:
			case 0x95: case 0x96: case 0x97: case 0x98: case 0x99:
			case 0x9a: case 0x9b: case 0x9c: case 0x9d: case 0x9e:
				parse90(op);
				break;

			case 0x9f:
				bufferAdd("%02d TONE %s", curLine++, decodeNN(getNextByte(), 1));
				break;

			case 0xa0: parseA0(op); break;
			case 0xa1: bufferAdd("%02d XROM ???", curLine++); break;
			case 0xa2: parseA2(op); break;
			case 0xa3: bufferAdd("%02d XROM ???", curLine++); break; break;
			case 0xa4: bufferAdd("%02d XROM ???", curLine++); break; break;
			case 0xa5: parseA5(op); break;
			case 0xa6: parseA6(op); break;
			case 0xa7: parseA7(op); break;

			case 0xa8: case 0xa9: case 0xaa: case 0xab: case 0xac:
			case 0xad:
				parseFlags(op);
				break;

			case 0xae:
				{
					uint8_t chr = getNextByte();

					if(chr & 0x80) {
						bufferAdd("%02d XEQ IND %s", curLine++, decodeNN(chr & 0x7f, 2));
					} else {
						bufferAdd("%02d GTO IND %s", curLine++, decodeNN(chr & 0x7f, 2));
					}
				}
				break;

			case 0xaf: bufferAdd("%02d af ???", curLine++); break;
			case 0xb0: bufferAdd("%02d b0 ???", curLine++); break;

			case 0xb1: case 0xb2: case 0xb3: case 0xb4: case 0xb5:
			case 0xb6: case 0xb7: case 0xb8: case 0xb9: case 0xba:
			case 0xbb: case 0xbc: case 0xbd: case 0xbe: case 0xbf:
				bufferAdd("%02d GTO %02d", curLine++, (op & 0x0f) - 1);
				getNextByte(); // Looks like we need to eat one byte.
				break;

			case 0xc0: case 0xc1: case 0xc2: case 0xc3: case 0xc4:
			case 0xc5: case 0xc6: case 0xc7: case 0xc8: case 0xc9:
			case 0xca: case 0xcb: case 0xcc: case 0xcd:
				if(parseLabel(op)) {
					// Program is complete.
					complete = true;
				}
				break;

			case 0xce: bufferAdd("%02d X<> %s", curLine++, decodeNN(getNextByte(), 2)); break;
			case 0xcf: parseShortLabel(op); break;

			case 0xd0: case 0xd1: case 0xd2: case 0xd3: case 0xd4:
			case 0xd5: case 0xd6: case 0xd7: case 0xd8: case 0xd9:
			case 0xda: case 0xdb: case 0xdc: case 0xdd: case 0xde:
			case 0xdf:
				// We need to eat one byte.  Also, these
				// prefixes are not used for IND commands,
				// hence we AND with 0x7f before decoding.
				getNextByte();
				bufferAdd("%02d GTO %s", curLine++, decodeNN(getNextByte() & 0x7f, 2));
				break;

			case 0xe0: case 0xe1: case 0xe2: case 0xe3: case 0xe4:
			case 0xe5: case 0xe6: case 0xe7: case 0xe8: case 0xe9:
			case 0xea: case 0xeb: case 0xec: case 0xed: case 0xee:
			case 0xef:
				// We need to eat one byte.  Also, these
				// prefixes are not used for IND commands,
				// hence we AND with 0x7f before decoding.
				getNextByte();
				bufferAdd("%02d XEQ %s", curLine++, decodeNN(getNextByte() & 0x7f, 2));
				break;

			case 0xf0:
				bufferAdd("%02d NOP", curLine++);
				break;
		       
			case 0xf1: case 0xf2: case 0xf3: case 0xf4: case 0xf5:
			case 0xf6: case 0xf7: case 0xf8: case 0xf9: case 0xfa:
			case 0xfb: case 0xfc: case 0xfd: case 0xfe: case 0xff:
				parseFn(op);
				break;
		}

		bufferEmit(pass, wantHex, savedPosition, getPosition());

		if(complete) {
			return;
		}
	}
}

void
usage()
{
	fprintf(stderr, "dm42decode - decode HP42 Calculator Programs\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "SYNOPSIS:\n");
	fprintf(stderr, "  dmdecode [OPTION] file.raw > file.txt\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -h, --help\n");
	fprintf(stderr, "       Display this help message\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -x, --hex\n");
	fprintf(stderr, "       Include a hexadecimal dump in the output\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "file.raw is the binary program to be decoded.\n");
	fprintf(stderr, "file.txt is the resulting decoded program, optionally.\n");
	fprintf(stderr, "with a hexadecimal representation included.\n");
}

int
main(int argc, char *argv[])
{
	int c;

	while(1) {
		int option_index = 0;
		static struct option long_options[] = {
			{"help",    no_argument,       0,  'h' },
			{"hex",     no_argument,       0,  'x' },
			{0,         0,                 0,  0 }
		};

		c = getopt_long(argc, argv, "x", long_options, &option_index);
		if(c == -1) {
			break;
		}

		switch(c) {
			case 'h':
				usage();
				exit(EXIT_SUCCESS);
				break;

			case 'x':
				wantHex = true;
				break;

			case '?':
				break;

			default:
				printf("?? getopt returned character code 0%o ??\n", c);
		}
	}

	if(optind < argc) {
		fileLoad(argv[optind++]);
	} else {
		fprintf(stderr, "Need input file name\n\n");
		usage();
		exit(EXIT_FAILURE);
	}

	int state = 0;

	while(sizeRemaining() > 0) {
		size_t startPoint;
		size_t endPoint;

		// First pass, to determine the program length.
		state = FIRST_PASS;
	       	startPoint = getPosition();
		curLine = 0;
		process(state, 0);
	       	endPoint = getPosition();

		// Second pass, to emit the data.  The program length does not
		// include the END statement, which is always 3 bytes long.
		state = SECOND_PASS;
		setPosition(startPoint);
		curLine = 0;
		process(state, endPoint - startPoint - END_LENGTH);

		// Separate sessions by a blank line.
		printf("\n");
	}

	exit(EXIT_SUCCESS);
}
